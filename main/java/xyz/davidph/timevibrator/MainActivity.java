package xyz.davidph.timevibrator;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {

    public static SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Switch toggleSwitch;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = pref.edit();
        seekbars((SeekBar)findViewById(R.id.speedSlider));
        seekbars((SeekBar)findViewById(R.id.pauseSlider));
    }
    private void checkPreferences(){
        String toggled;

        CheckBox fbox = findViewById(R.id.checkBox);
        toggled = pref.getString(getString(R.string.foregroundSwitch), "True");
        System.out.println("here");
        if(toggled != null) {
            System.out.println("here2");
            if (toggled.equals("True"))
                fbox.setChecked(true);
            else
                fbox.setChecked(false);
        }
        toggled = pref.getString(getString(R.string.enableSwitch), "True");
        if(toggled != null) {
            boolean run = toggled.equals("True");
            if (runService(run))
                toggleSwitch.setChecked(true);
            else
                toggleSwitch.setChecked(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                toggleSwitch = findViewById(R.id.ToggleSwitch);
                checkPreferences();

                toggleSwitch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(runService(toggleSwitch.isChecked()))
                            editor.putString(getString(R.string.enableSwitch), "True");
                        else
                            editor.putString(getString(R.string.enableSwitch), "False");

                        editor.apply();
                    }
                });
            }
        });
        return true;
    }

    public boolean runService(boolean run) {
        Intent intent = new Intent(this, service.class);
        if (run) {
            startService(intent);
        } else
            stopService(intent);
        return run;
    }

    public void click(View view) {
        Clock.vibrateTime(this);
    }

    public void click2(View view) { runService(false); }

    public void foregroundBox(View view) {
        CheckBox fbox = findViewById(R.id.checkBox);
        if(fbox.isChecked())
            editor.putString(getString(R.string.foregroundSwitch), "True");
        else
            editor.putString(getString(R.string.foregroundSwitch), "False");
        if(toggleSwitch.isChecked()){
            toggleSwitch.setChecked(false);
            toggleSwitch.callOnClick();
        }
        editor.apply();
    }

    public void seekbars(final SeekBar s) {
        s.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if(s.getId() == R.id.speedSlider)
                            Clock.changeSpeed(s.getProgress() / 100);
                        else if(s.getId() == R.id.pauseSlider)
                            Clock.changeDelay(s.getProgress() / 100);
                    }
                }
        );
    }
}
