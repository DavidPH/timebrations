package xyz.davidph.timevibrator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Receiver extends BroadcastReceiver {
    private long dur = 0;

    private boolean phoneCall(Context context) {
        TelephonyManager phone = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return phone != null &&
                (phone.getCallState() == TelephonyManager.CALL_STATE_OFFHOOK ||
                        phone.getCallState() == TelephonyManager.CALL_STATE_RINGING);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("Received Intent");
        Log.d("Receiver","Intent: " + intent.getAction());
        if (intent.getAction() != null) {
            if (!phoneCall(context)) {

                Intent tempIntent = new Intent(context, service.class);
                if (intent.getAction().equals(Intent.ACTION_SCREEN_ON))
                    dur = System.currentTimeMillis();
                else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                    long tolerance = 800;
                    long diff = System.currentTimeMillis() - dur;
                    Log.d("onReceive()", "dur: " + diff);
                    if (diff < tolerance) {
                        tempIntent.addCategory("VIBRATE_CATEGORY");
                        context.startService(tempIntent);
                    }
                }
            }
        }
    }
}
