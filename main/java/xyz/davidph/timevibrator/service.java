package xyz.davidph.timevibrator;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

public class service extends Service {
    public NotificationManagerCompat nmang;
    private Receiver receiver;
    @Override
    public IBinder onBind(Intent arg) {
        return null;
    }
//
    public Notification makeNotif(String text, int icon){
        Intent goTo = new Intent(this, MainActivity.class);
        PendingIntent intent = PendingIntent.getActivity(this, 0, goTo, 0);
        return new NotificationCompat.Builder(this, App.CHANNEL_1_ID)
                .setSmallIcon(icon)
                .setContentTitle(text)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setContentIntent(intent).build();
    }


    @Override
    public void onCreate() {
        nmang = NotificationManagerCompat.from(this);
        super.onCreate();
        Toast.makeText(this,getResources().getString(R.string.app_name) + " Started", Toast.LENGTH_SHORT).show();
        Log.d("service.onCreate", "Service started");
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        this.receiver = new Receiver();
        registerReceiver(receiver, filter);

        if(MainActivity.pref.getString(getString(R.string.foregroundSwitch), "True").equals("True"))
            startForeground(1, makeNotif("Running", R.drawable.ic_clock));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("service.onStart()", "Service called with "+ intent.getAction());
//        nmang.notify(2, makeNotif("Called", R.drawable.ic_stopped));
        if(intent.hasCategory("VIBRATE_CATEGORY")){
            Clock.vibrateTime(this);
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, getResources().getString(R.string.app_name) + " Stopped", Toast.LENGTH_SHORT).show();
        nmang.notify(1, makeNotif("SERVICE STOPPED", R.drawable.ic_stopped));
        Log.d("service.onDestroy()", "Service stopped");
        if (receiver != null)
            unregisterReceiver(receiver);

    }


}
