package xyz.davidph.timevibrator;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

class Clock {
    private static long[] hourPattern = {250, 450};
    private static long[] tensPattern = {150, 300};
    private static long[] unitsPattern = {150, 250};

    private static long tensDelay = 350;
    private static long unitsDelay = 750;

    public static void changeDelay(long p){
        changeDelay(p,p);
    }

    public static void changeDelay(long t, long u){
        tensDelay *= t;
        unitsDelay *= u;
    }

    public static void changeSpeed(long p){
        changeSpeed(p,p,p);
    }

    public static void changeSpeed(long h, long m, long u){
        long[] hp = {250, 450};
        long[] tp= {150, 300};
        long[] up = {150, 250};

        System.out.println("h " + hourPattern[0]);
        System.out.println("h " + hourPattern[1]);
        hourPattern[0] = hp[0] * h;
        hourPattern[1] = hp[1] * h;

        tensPattern[0] = tp[0] * m;
        tensPattern[1] = tp[1] * m;
        System.out.println("m " +tensPattern[0]);
        System.out.println("m " +tensPattern[1]);

        unitsPattern[0] = up[0] * u;
        unitsPattern[1] = up[1] * u;
        System.out.println("u "+tensPattern[0]);
        System.out.println("u "+tensPattern[1]);

    }

    private static long[] makePattern() {
        //TODO add logic for 12 PM/AM and for 0 units
        Calendar calendar = new GregorianCalendar();
        final int hours = calendar.get(Calendar.HOUR);
        final int minutes = calendar.get(Calendar.MINUTE);

        List<Long> lst = new ArrayList<>();
        lst.add(0L);
        for (int i = 0; i < hours; i++) {
            for (long num : hourPattern) {
                lst.add(num);
            }
        }
        lst.add(0L);
        lst.add(tensDelay);
        for (int i = 0; i < (minutes / 10) % 10; i++) {
            for (long num : tensPattern) {
                lst.add(num);
            }
        }
        lst.add(0L);
        lst.add(unitsDelay);
        for (int i = 0; i < minutes % 10; i++) {
            for (long num : unitsPattern) {
                lst.add(num);
            }
        }

        long[] pattern = new long[lst.size()];
        for (int i = 0; i < lst.size(); i++) pattern[i] = lst.get(i);

        return pattern;
    }

    static void vibrateTime(final Context context) {
        final android.os.Vibrator v = (android.os.Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = makePattern();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            v.vibrate(VibrationEffect.createWaveform(pattern, -1));
        else
            v.vibrate(pattern, -1);
    }
}
